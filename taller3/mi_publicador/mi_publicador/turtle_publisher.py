from geometry_msgs.msg import Twist
import rclpy
from rclpy.node import Node

class TurtlePublisher(Node):

    def __init__(self):
        super().__init__('turtle_publisher')
        self.publisher_ = self.create_publisher(Twist, 'turtle1/cmd_vel', 10)
        timer_period = 1.0  # segundos
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        msg = Twist()
        msg.linear.x = 8.0  # Velocidad lineal en el eje x
        msg.angular.z = 7.0  # Velocidad angular en el eje z
        self.publisher_.publish(msg)
        self.get_logger().info('Publicando velocidad: linear=%.1f, angular=%.1f' % (msg.linear.x, msg.angular.z))

def main(args=None):
    rclpy.init(args=args)
    turtle_publisher = TurtlePublisher()
    rclpy.spin(turtle_publisher)
    turtle_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
