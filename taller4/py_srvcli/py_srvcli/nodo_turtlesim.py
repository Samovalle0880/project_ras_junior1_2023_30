import rclpy
from turtlesim.srv import Kill, Spawn, SetPen, TeleportRelative

def main():
    rclpy.init()
    node = rclpy.create_node('nodo_turtlesim')

    # Crear clientes de servicio para los servicios necesarios
    client_clear = node.create_client(Kill, 'clear')
    client_spawn = node.create_client(Spawn, 'spawn')
    client_set_pen = node.create_client(SetPen, 'set_pen')
    client_teleport_relative = node.create_client(TeleportRelative, 'teleport_relative')

    while not client_clear.wait_for_service(timeout_sec=3.0):
        node.get_logger().info('Servicio clear no disponible, esperando...')
    clear_request = Kill.Request()
    future_clear = client_clear.call_async(clear_request)
    rclpy.spin_until_future_complete(node, future_clear)

    spawn_request = Spawn.Request()
    spawn_request.x = 5
    spawn_request.y = 0
    spawn_request.theta = 1
    future_spawn = client_spawn.call_async(spawn_request)
    rclpy.spin_until_future_complete(node, future_spawn)

    # Set pen to red
    set_pen_request = SetPen.Request()
    set_pen_request.r = 255
    set_pen_request.g = 0
    set_pen_request.b = 0
    set_pen_request.width = 5
    set_pen_request.off = False
    future_set_pen = client_set_pen.call_async(set_pen_request)
    rclpy.spin_until_future_complete(node, future_set_pen)

    # Teleport_relative to x:0, y:0, theta:0
    teleport_request = TeleportRelative.Request()
    teleport_request.linear = 0
    teleport_request.angular = 0
    future_teleport = client_teleport_relative.call_async(teleport_request)
    rclpy.spin_until_future_complete(node, future_teleport)

    # Teleport_relative that moves linear 2 positions
    teleport_request.linear = 2
    future_teleport = client_teleport_relative.call_async(teleport_request)
    rclpy.spin_until_future_complete(node, future_teleport)

    # Spawn "mrsTurtle2" with x:2, y:2, theta: 0
    spawn_request.x = 2
    spawn_request.y = 2
    spawn_request.theta = 0
    future_spawn = client_spawn.call_async(spawn_request)
    rclpy.spin_until_future_complete(node, future_spawn)

    # Set pen off
    set_pen_request.off = True
    future_set_pen = client_set_pen.call_async(set_pen_request)
    rclpy.spin_until_future_complete(node, future_set_pen)

    # Teleport_relative to x:5, y:5, theta:0
    teleport_request.linear = 5
    teleport_request.angular = 0
    future_teleport = client_teleport_relative.call_async(teleport_request)
    rclpy.spin_until_future_complete(node, future_teleport)

    # Teleport_relative that moves angular 2 degrees
    teleport_request.linear = 0
    teleport_request.angular = 2
    future_teleport = client_teleport_relative.call_async(teleport_request)
    rclpy.spin_until_future_complete(node, future_teleport)

    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
