# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from geometry_msgs.msg import Twist  # Importa el tipo de mensaje Twist

import rclpy
from rclpy.node import Node

class MinimalSubscriber(Node):

    def __init__(self):
        super().__init__('minimal_subscriber')
        self.subscription = self.create_subscription(
            Twist,  # Escucha mensajes de tipo Twist
            'topic',
            self.listener_callback,
            10)
        self.subscription  # Evita la advertencia de variable no utilizada

    def listener_callback(self, msg):
        linear = msg.linear
        angular = msg.angular
        self.get_logger().info(
            'Recibido: "Linear part x=%.1f y=%.1f z=%.1f, Angular x=%.1f y=%.1f z=%.1f"' %
            (linear.x, linear.y, linear.z, angular.x, angular.y, angular.z))

def main(args=None):
    rclpy.init(args=args)

    minimal_subscriber = MinimalSubscriber()

    rclpy.spin(minimal_subscriber)

    minimal_subscriber.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
