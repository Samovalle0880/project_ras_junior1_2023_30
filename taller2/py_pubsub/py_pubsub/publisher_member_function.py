# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from geometry_msgs.msg import Twist  # Importa el tipo de mensaje Twist

import rclpy
from rclpy.node import Node

class MinimalPublisher(Node):

    def __init__(self):
        super().__init__('minimal_publisher')
        self.publisher_ = self.create_publisher(Twist, 'topic', 10)  # Utiliza Twist como tipo de mensaje
        timer_period = 0.5  # segundos
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        msg = Twist()  # Crea un mensaje de tipo Twist

        # Establece los valores para la parte lineal (Linear) del mensaje Twist
        msg.linear.x = 0.0
        msg.linear.y = 2.1
        msg.linear.z = 1.3

        # Establece los valores para la parte angular (Angular) del mensaje Twist
        msg.angular.x = 0.1
        msg.angular.y = 2.5
        msg.angular.z = 1.8

        self.publisher_.publish(msg)
        self.get_logger().info('Publicando: "Linear part x=%.1f y=%.1f z=%.1f, Angular x=%.1f y=%.1f z=%.1f"' %
                               (msg.linear.x, msg.linear.y, msg.linear.z,
                                msg.angular.x, msg.angular.y, msg.angular.z))

def main(args=None):
    rclpy.init(args=args)

    minimal_publisher = MinimalPublisher()

    rclpy.spin(minimal_publisher)

    minimal_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
