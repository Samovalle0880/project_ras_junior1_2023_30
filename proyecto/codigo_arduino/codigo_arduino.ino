#include <Servo.h>

#define base_pin 8
#define leftside_pin 9
#define rightside_pin 10
#define gripper_pin 11

Servo base_motor;
Servo leftside_motor;
Servo rightside_motor;
Servo gripper_motor;

void setup() {
  // put your setup code here, to run once:
  base_motor.attach(base_pin);
  leftside_motor.attach(leftside_pin);
  rightside_motor.attach(rightside_pin);
  gripper_motor.attach(gripper_pin);

  Serial.begin(115200);

  base_motor.write(0);
  leftside_motor.write(0);
  rightside_motor.write(0);
  gripper_motor.write(0);
  pinMode(LED_BUILTIN,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  int Servo1,Servo2,Servo3,Servo4;
  if(Serial.available() > 0){
    String message = Serial.readString();
    int index_coma = 0;
    int from = 0;
    index_coma = message.indexOf(',');
    Servo1 = message.substring(from,index_coma).toInt();
    from = index_coma+1;
    index_coma = message.indexOf(',',from);
    Servo2 = message.substring(from,index_coma).toInt();
    from = index_coma+1;
    index_coma = message.indexOf(',',from);
    Servo3 = message.substring(from,index_coma).toInt();
    from = index_coma+1;
    index_coma = message.indexOf(',',from);
    Servo4 = message.substring(from,index_coma).toInt();

    base_motor.write(Servo1);
    leftside_motor.write(Servo2);
    rightside_motor.write(Servo3);
    gripper_motor.write(Servo4);
    if(Servo1 == 1 &&  Servo2 ==2 &&  Servo3 ==3 &&  Servo4 ==4){
      digitalWrite(LED_BUILTIN,HIGH);  
    } else {
      digitalWrite(LED_BUILTIN,LOW);
    }
  }
  delay(50);
}
